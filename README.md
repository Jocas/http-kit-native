# http-kit-native

Demo project to create a native executable using GraalVM for a Clojure app that uses http-kit.

The showcase is to make an HTTP GET request to get the Elasticsearch status.

## TL;DR

```
$ make docker-native-image
$ ./es-util
```

## Clojure CLI utility

The utility can be invoked as a regular clj utility:
```
clj -m es-utils.core
```
Produces output:
```
{:opts {:method :get, :headers {Content-Type application/json}, :url http://localhost:9200}, :body {
  "name" : "-yc7hn8",
  "cluster_name" : "docker-cluster",
  "cluster_uuid" : "xLBZAUvITWefFRGCM0IpZw",
  "version" : {
    "number" : "6.3.1",
    "build_flavor" : "oss",
    "build_type" : "tar",
    "build_hash" : "eb782d0",
    "build_date" : "2018-06-29T21:59:26.107521Z",
    "build_snapshot" : false,
    "lucene_version" : "7.3.1",
    "minimum_wire_compatibility_version" : "5.6.0",
    "minimum_index_compatibility_version" : "5.0.0"
  },
  "tagline" : "You Know, for Search"
}
, :headers {:content-encoding gzip, :content-length 297, :content-type application/json; charset=UTF-8}, :status 200}

```

## Time
```
$ clj -m es-utils.core
# => 4.47s user 0.14s system 207% cpu 2.221 total
$ ./es-util
# => 0.00s user 0.01s system 89% cpu 0.016 total

```
