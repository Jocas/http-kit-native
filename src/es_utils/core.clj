(ns es-utils.core
  (:gen-class)
  (:require [org.httpkit.client :as http])
  (:import [java.net URI]
           [javax.net.ssl SNIHostName SSLEngine SSLParameters]))

(defn sni-configure
  [^SSLEngine ssl-engine ^URI uri]
  (let [^SSLParameters ssl-params (.getSSLParameters ssl-engine)]
    (.setServerNames ssl-params [(SNIHostName. (.getHost uri))])
    (.setSSLParameters ssl-engine ssl-params)))

(def sni-client (delay (http/make-client {:ssl-configurer sni-configure})))

(defn -main [& args]
  (println
    @(http/request
       {:method  :get
        :client  @sni-client
        :headers {"Content-Type" "application/json"}
        :url     (or (first args) "http://localhost:9200")})))
