docker-native-image:
	docker build -f Dockerfile -t registry.gitlab.com/jocas/http-kit-native .
	docker rm build || true
	docker create --name build registry.gitlab.com/jocas/http-kit-native
	docker cp build:/usr/src/app/target/app es-util
	docker cp build:/usr/src/app/libsunec.so libsunec.so
